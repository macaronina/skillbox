import React from 'react';
import styles from './list.css';

interface IItem{
  value:string,
  id:string
}

interface IListProps{
  list:IItem[];
}

export function List({list}:IListProps) {
  return (
<ul>
  {list.map((item:IItem, index:number)=>(
    <li key={item.id}>{item.value}</li>
  ))}
</ul>
  );
}
