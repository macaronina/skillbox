import React from 'react';
import { BlockIcon, CommentButton, SaveIcon, ShareButton, WarningIcon } from '../../../../Icons';
import styles from './menuitemslist.css';
import { Text } from '../../../../Text';
import { generateRandomString, assighId, generateId } from '../../../../../utils/react/generateRandomIndex';
import { GenericList } from '../../../../GenericList';
import { EIcons, Icon } from '../../../../Icon';

//<CommentButton />



const LIST = [
  {content:[<Icon name={EIcons.comment} size={16}/>,<Text sizes={12} desctopSize={14}>Комментарии</Text>], className: `${styles.menuItem}`},
  { className: `${styles.divider}` },
  {content:[<Icon name={EIcons.share} size={16}/>,<Text sizes={12} desctopSize={14}>Поделиться</Text>], className: `${styles.menuItem + ' ' + styles.desctopVisible}`},
  { className: `${styles.divider}` },
  {content:[<Icon name={EIcons.block} size={16}/>,<Text sizes={12} desctopSize={14}>Скрыть</Text>], className: `${styles.menuItem}`},
  { className: `${styles.divider}` },
  {content:[<Icon name={EIcons.save} size={16}/>,<Text sizes={12} desctopSize={14}>Сохранить</Text>], className: `${styles.menuItem + ' ' + styles.desctopVisible}`},
  { className: `${styles.divider}` },
  {content:[<Icon name={EIcons.warning} size={16}/>,<Text sizes={12} desctopSize={14}>Пожаловаться</Text>], className: `${styles.menuItem}`}
].map(generateId);

export function MenuItemslist() {
  return (
    <ul className={styles.menuItemsList}>

      <GenericList list={LIST} />

    </ul>
  );
}
