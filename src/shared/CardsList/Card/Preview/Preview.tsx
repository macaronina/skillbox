import React from 'react';
import styles from './preview.css';

export function Preview() {
  return (
    <div className={styles.preview}>
      <img src="https://cdn.dribbble.com/userupload/4461907/file/original-886b29a656349f4da314094659eccb24.png?compress=1&resize=640x480&vertical=top" className={styles.previewImg} />
    </div>
  );
}
