import React from 'react';
import styles from './userlink.css';

export function UserLink() {
  return (
    <div className={styles.userLink}>
      <img src='https://cdn.dribbble.com/users/1723105/avatars/normal/f90425344edb0679db2fe8ca9726ae47.png?1650002980' className={styles.avatar} />
      <a href="#user" className={styles.username}>Константин Кодов</a>
    </div>
  );
}
