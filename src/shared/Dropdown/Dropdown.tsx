import React from 'react';
import styles from './dropdown.css';


interface IDropdownProps {
  button: React.ReactNode;
  children: React.ReactNode;
}

export function Dropdown({ button, children }: IDropdownProps) {

  const [IsDropdownOpen, setIsDropdownOpen] = React.useState(false);
  return (
    <div className={styles.container}>
      <div onClick={() => setIsDropdownOpen(!IsDropdownOpen)}>
        {button}
      </div>
      {IsDropdownOpen && (
        <div className={styles.listContainer}>
          <div className={styles.list} onClick={()=>setIsDropdownOpen(false)}>
            {children}
          </div>
        </div>
      )}
    </div>
  );
}
