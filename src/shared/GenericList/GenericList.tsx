import React from 'react';
import styles from './genericlist.css';


interface IItem {
  content?: React.ReactNode;
 // icon?: React.ReactNode;
  id: string;
  className?: string;
  As?: 'a' | 'li' | 'button' | 'div' | 'span';
  href?: string;
  //text?:string;
}

interface IGenericListProps {
  list: IItem[];
}

export function GenericList({ list }: IGenericListProps) {
  return (
    <>
      {list.map(({ As = 'li', content, className, id, href }) => (
        <As
          className={className}
          key={id}
          href={href}
        >
         {content}
        </As>
      ))}
    </>
  );
}
