import React from 'react';
import { BlockIcon, CommentButton, SaveIcon, ShareButton, WarningIcon } from '../Icons';
import styles from './icon.css';
import classnames from 'classnames';

export enum EIcons {
  comment = 'comment',
  block='block',
  save='save',
  share='share',
  warning='warning'
}

type TSizes =  16 | 14 | 12 | 10;

function getIconComponent(name:EIcons){
  switch(name){
    case EIcons.comment: return <CommentButton/>
    case EIcons.block: return <BlockIcon/>
    case EIcons.save: return <SaveIcon/>
    case EIcons.share: return <ShareButton/>
    case EIcons.warning: return <WarningIcon/>
  }
}

interface IIconProp {
  name: EIcons;
  size?:TSizes;
}

export function Icon(props: IIconProp) {
  const { name, size } = props;

   const classes = classnames(
    styles[`size${ size }`]
  );

  return (
    <span className={classes}>{getIconComponent(name)}</span>
  );
}
