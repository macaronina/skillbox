import React from 'react';
import styles from './text.css';
import classnames from 'classnames';

type TSizes = 28 | 20 | 16 | 14 | 12 | 10;

export enum EColor {
  black = 'black',
  orange = 'orange',
  white = 'white',
  gray='gray'
}

interface ITextProps {
  As?: 'h1' | 'h2' | 'h3' | 'h4' | 'span' | 'div' | 'p';
  children?: React.ReactNode;
  sizes: TSizes;
  mobileSize?: TSizes;
  tabletSize?: TSizes;
  desctopSize?: TSizes;
  color?: EColor;
}

export function Text(props: ITextProps) {

  const { 
    As = 'span', 
    children, 
    sizes, 
    mobileSize, 
    tabletSize, 
    desctopSize,
    color=EColor.black
  } = props;

  const classes = classnames(
    styles[`s${ sizes }`],
    {[styles[`m${mobileSize}`]]:mobileSize},
    {[styles[`d${desctopSize}`]]:desctopSize},
    {[styles[`t${tabletSize}`]]:tabletSize},
    styles[color]
  );
 
  return (
    <As className={classes}>
      {children}
    </As>
  );
}
