import React from 'react';
import styles from './content.css';

interface IContenProps{
  children?: React.ReactNode;
}

export function Content({children}: IContenProps) {
  return (
   <main className={styles.content}>
     {children}
   </main>
  );
}
