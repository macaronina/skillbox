import React from 'react';
import { Icon } from '../../../Icon';
import { IconAnon } from '../../../Icons';
import { EColor, Text } from '../../../Text';
import styles from './userblock.css';

interface IUserBlockProps {
  avatarSrc?: string;
  username?: string;
}


export function UserBlock({ avatarSrc, username }: IUserBlockProps) {
  return (
    <a href="https://www.reddit.com/api/v1/authorize?client_id=_iMM6iTOU-w4OL2k7mtT2A&response_type=code&state=random_string&redirect_uri=http://localhost:3000/auth&duration=permanent&scope=read submit identity" className={styles.userBox}>
      <div className={styles.avatarBox}>
        {
          avatarSrc ?
            <img src={avatarSrc} className={styles.avatarImage} alt="user avatar"/>
            :<IconAnon />
        }
      </div>
      <div className={styles.username}>
        <Text sizes={20} color={username?EColor.black:EColor.gray}>{username || 'Аноним'}</Text>
      </div>
    </a>
  )
}
