
//1. Работа с простыми типами
//Напишите тип функции, конкатенирующей две строки


function concat(a:string,b:string):string{
    return a+b
};
concat('Hello ', 'World') // -> Hello World;


//2. Работа с интерфейсами

interface IMyHometask {
    howIDoIt: string,
    simeArray: Array<string | number>,
    withData?: Array<IMyHometask>
}

const MyHometask: IMyHometask = {

    howIDoIt: "I Do It Wel",

    simeArray: ["string one", "string two", 42],

    withData: [{ howIDoIt: "I Do It Wel", simeArray: ["string one", 23] }],

}


//3. Типизация функций, используя Generic


interface MyArray<T> {

    [N: number]: T;

    //добавьте типизацию для метода reduce
    reduce<U>(fn:(acc:U, val:T, index:number, array:T[] )=> U, initial:U):U

}


const initialValue = 0;
[1,2,4].reduce((accumulator, value) => accumulator + value, initialValue);


//4. Работа с MappedTypes

interface IHomeTask {
    data: string;
    numbericData: number;
    date: Date;
    externalData: {
        basis: number;
        value: string;
    }
}

const homeTask: MyPartial<IHomeTask> = {
    externalData: {
        value: 'win'
    }
}


type MyPartial<T> = {
    [N in keyof T]?: T[N] extends object ? MyPartial<T[N]> : T[N]
}


